# README.md

## Pronunciation and meaning
`Xipat` sounds like in 'she-pat!' and it means *nice* in [Munduruku](https://en.wikipedia.org/wiki/Munduruku_people) language.

## What it does
Simple script to export subtitles from FinalCut nXML format file to different standard of web subtitle formats.

