# xipat.py
from bs4 import BeautifulSoup as bs
from datetime import timedelta
from decimal import *


# --- Helper functions
def from_frames_to_ms(frames):
    division = Decimal(frames) / Decimal('30.000')
    decimal_part = str(division % Decimal('1.000'))[2:5]
    total_seconds = int(division)
    return ("%s,%s" % (str(timedelta(seconds=total_seconds)), decimal_part))

# Main program

print("Initialization...")
# Open the file and prepare the soup
soup = bs(open("test.xml", 'r'), 'xml')
print("Soup prepared!")

# Select all generator items
gens = soup.select("track generatoritem")

effects = [x.effect for x in gens]

subtitles_effects = [x for x in effects if x.select('name')[0].text == "Text"]

subtitle_dicts = [{'start': x.parent.select('start')[0].text,
                 'end': x.parent.select('end')[0].text,
                 'text': x.select('parameter value')[0].text} for x in subtitles_effects]


with open('test.srt', 'w') as fileio:
    for sub in subtitle_dicts:
        print(subtitle_dicts.index(sub)+1, file=fileio)
        print(from_frames_to_ms(sub['start'])+" --> "+from_frames_to_ms(sub['end']), file=fileio)
        print(sub['text'], file=fileio)
        print("", file=fileio)


